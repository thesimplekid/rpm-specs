#
# spec file for package bisq-desktop
#
# Copyright (c) 2022 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#


Name:           bisq
Version:       	1.8.0 
Release:        1
Summary:        Bisq is a safe, private and decentralized way to exchange bitcoin for national currencies and other digital assets.
License:        AGPL-3.0
URL:            https://bisq.network/
Source0:        %{name}-%{version}.tar.gz
BuildRequires:  java-15-openjdk
ExclusiveArch: 	x86_64

%description
Bisq is a safe, private and decentralized way to exchange bitcoin for national currencies and other digital assets. Bisq uses peer-to-peer networking and multi-signature escrow to facilitate trading without a third party. Bisq is non-custodial and incorporates a human arbitration system to resolve disputes.

%prep
%setup -q

%build
./gradlew :desktop:build

%install
# install executable
install -Dpm 0755 bisq-desktop %{buildroot}/opt/%{name}/bin/bisq
mkdir -p %{buildroot}/opt/%{name}/lib/app/
cp desktop/build/libs/desktop-%{version}-all.jar %{buildroot}/opt/%{name}/lib/app/desktop-%{version}-all.jar
# install -d %{buildroot}/opt/bisq
# cp -pr desktop/build/app/* %{buildroot}/opt/%{name}
# rm %{buildroot}/opt/bisq/bin/bisq-desktop.bat
install -d %{buildroot}/usr/bin

#install desktop launcher
# install -Dm644 bisq.desktop %{buildroot}%{_datadir}/applications/%{name}.desktop
# install -Dm644 desktop/package/linux/icon.png %{buildroot}%{_datadir}/icons/128x128/apps/%{name}.png

%postun
%desktop_database_postun
%icon_theme_cache_postun

%files
/opt/%{name}/bin/*
/opt/%{name}/lib/*

# %{_datadir}/applications/bisq.desktop
# %{_datadir}/icons/*/apps/%{_name}.png

%license LICENSE

%changelog
