#
# spec file for package tutanota-desktop
#
# Copyright (c) 2022 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#

%define nodejs_min_version 16
%define npm_min_version 7

%define base tutanota
%define unpacked_folder %{base}-%{base}-release-%{version}/

Name:           tutanota-desktop           
Version:        3.91.2
Release:        34
Summary:        Desktop app for tutanota
License:        GPL-3.0
URL:            https://tutanota.com/
Source0: 	%{base}-%{base}-release-%{version}.tar.gz
Source1:        %{name}.desktop
Source2:        LICENSE

BuildRequires: 	libsecret-devel
BuildRequires:  gcc-c++
BuildRequires:  fdupes
BuildRequires:  nodejs >= %{nodejs_min_version}
BuildRequires:  npm >= %{npm_min_version}
ExclusiveArch:  x86_64

%description
Desktop application for tutanota. A secure email service with built-in end-to-end encryption that enables you to communicate securely with anyone on all your devices.

%prep
#Untar source
tar -xf %{SOURCE0}


%build
# cd %{base}-%{base}-release-%{version}/
cd %{unpacked_folder}
npm ci
npm run build-packages
node dist --custom-desktop-release

%install
ls
# cd into linux unpacked dir
cd %{unpacked_folder}build/desktop/linux-unpacked/

install -d %{buildroot}/opt/%{name}
cp -pr * %{buildroot}/opt/%{name}

# install desktop file
install -d -m 0755 %{buildroot}%{_datadir}/applications/
install -D -m 0644 %{SOURCE1} %{buildroot}%{_datadir}/applications/%{name}.desktop

#Copy License
cp %{SOURCE2} %{buildroot}/opt/%{name}/

%fdupes %{buildroot}/opt/%{name}

%files
%license /opt/%{name}/LICENSE

%dir /opt/%{name}/
/opt/%{name}/*

%dir %{_datadir}/applications/
%{_datadir}/applications/%{name}.desktop


%changelog
